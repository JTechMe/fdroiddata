Categories:System
License:GPLv3
Web Site:http://www.batterybot.info/
Source Code:https://github.com/darshan-/Battery-Indicator-Free
Issue Tracker:https://github.com/darshan-/Battery-Indicator-Free/issues
Donate:https://github.com/darshan-/Battery-Indicator-Support/blob/HEAD/PackageDownload.md#support--donate

Auto Name:BatteryBot
Summary:Battery monitoring tool
Description:
Shows your battery charge level (percent) as an icon in your status bar, with
temperature, health, voltage, and time since plugged / unplugged in the
notification area. It also has user-configurable alarms, logs, and widgets.
.

Repo Type:git
Repo:https://github.com/darshan-/Battery-Indicator-Free

Build:9.0.1,11200
    commit=b20bb760b956b4082edbb04ea9b2f217c441fa64
    target=android-22

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:9.0.1
Current Version Code:11200
