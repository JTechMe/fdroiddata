Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Draw
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Draw/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Draw/blob/HEAD/CHANGELOG.md

Auto Name:Simple Draw
Summary:A canvas you can draw on with different colors
Description:
Want to draw something but you have no paper? This app will suit you perfectly.
Just pick a color and draw. You can either save the drawing or share it directly
through email or social networks.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Draw

Build:1.6,6
    commit=a85f4b6596b3d46072030a98e030795abaf28d11
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.6
Current Version Code:6
