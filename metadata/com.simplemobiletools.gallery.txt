Categories:Multimedia
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Gallery
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Gallery/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Gallery/blob/HEAD/CHANGELOG.md

Auto Name:Simple Gallery
Summary:A gallery for viewing photos and videos
Description:
Albums are sorted by the modified time of the last item in them from the latest
to oldest. Photos are zoomable, they can be previewed in multiple columns
depending on the size of the display and portrait/landscape mode. They can be
renamed, shared or deleted.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Gallery

Build:1.9,10
    commit=33909cbba4ca354351713077d72b42ae611209af
    subdir=app
    gradle=yes

Build:1.11,11
    commit=1.11
    subdir=app
    gradle=yes

Build:1.12,12
    commit=1.12
    subdir=app
    gradle=yes

Build:1.13,13
    commit=1.13
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.13
Current Version Code:13
